export default function cart(state = [], action) {
  switch (action.type) {
    case '@cart/ADD': {
      const { id } = action.payload;
      const productFound = state.find(product => product.id === id);

      if (productFound) {
        const newCart = state.filter(product => product.id !== productFound.id);
        return [
          ...newCart,
          {
            ...productFound,
            amount: productFound.amount + 1,
          },
        ];
      }
      return [
        ...state,
        {
          ...action.payload,
          amount: 1,
        },
      ];
    }

    case '@cart/REMOVE': {
      const { id } = action.payload;
      const cartUpdated = state.filter(product => product.id !== id);
      return cartUpdated;
    }

    case '@cart/UPDATE_AMOUNT': {
      const { id, amount } = action.payload;
      console.log(amount);
      const amountNumber = Number(amount);
      if (amountNumber < 1) {
        return state;
      }
      const productIndex = state.findIndex(product => product.id === id);
      if (productIndex >= 0) {
        const newState = state;
        newState[productIndex] = {
          ...newState[productIndex],
          amount: amountNumber,
        };
        return newState;
      }
    }

    default:
      return state;
  }
}
