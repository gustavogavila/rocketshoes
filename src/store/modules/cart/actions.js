export function add(product) {
  return { type: '@cart/ADD', payload: product };
}

export function remove(productId) {
  return { type: '@cart/REMOVE', payload: { id: productId } };
}

export function updateAmount(id, amount) {
  return { type: '@cart/UPDATE_AMOUNT', payload: { id, amount } };
}
