import React, { Component } from 'react';
import {
  MdAddCircleOutline,
  MdDelete,
  MdRemoveCircleOutline,
} from 'react-icons/md';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { remove, updateAmount } from '../../store/modules/cart/actions';
import { Container, EmptyCart, ProductTable, Total } from './styles';

class Cart extends Component {
  handleDeleteProduct = id => {
    const { remove } = this.props;
    remove(id);
  };

  handleAddAmount = product => {
    console.log(product);
    const { updateAmount } = this.props;
    const { id, amount } = product;
    updateAmount(id, amount + 1);
  };

  handleRemoveAmount = product => {
    const { updateAmount } = this.props;
    const { id, amount } = product;
    updateAmount(id, amount - 1);
  };

  render() {
    const { cart } = this.props;
    return (
      <Container>
        {cart && cart.length > 0 ? (
          <>
            <ProductTable>
              <thead>
                <tr>
                  <th />
                  <th>PRODUTO</th>
                  <th>QTD</th>
                  <th>SUBTOTAL</th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {cart.map(product => (
                  <tr key={product.id}>
                    <td>
                      <img src={product.image} alt={product.title} />
                    </td>
                    <td>
                      <strong>{product.title}</strong>
                      <span>{product.priceFormatted}</span>
                    </td>
                    <td>
                      <div>
                        <button type="button">
                          <MdRemoveCircleOutline
                            color="#7159c1"
                            size={20}
                            onClick={() => this.handleRemoveAmount(product)}
                          />
                        </button>
                        <input type="number" readOnly value={product.amount} />
                        <button type="button">
                          <MdAddCircleOutline
                            color="#7159c1"
                            size={20}
                            onClick={() => this.handleAddAmount(product)}
                          />
                        </button>
                      </div>
                    </td>
                    <td>
                      <strong>{2000.0}</strong>
                    </td>
                    <td>
                      <button type="button">
                        <MdDelete
                          color="#7159c1"
                          size={20}
                          onClick={() => this.handleDeleteProduct(product.id)}
                        />
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </ProductTable>

            <footer>
              <button type="button">Finalizar pedido</button>
              <Total>
                <span>TOTAL</span>
                <strong>R$1920,28</strong>
              </Total>
            </footer>
          </>
        ) : (
          <EmptyCart>Seu carrinho está vazio :(</EmptyCart>
        )}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  cart: state.cart,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ remove, updateAmount }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
